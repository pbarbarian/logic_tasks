public class Path {
    private String path;

    public Path(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void cd(String newPath) {
        if (newPath.startsWith("..")) {
            path = path.substring(0, path.lastIndexOf("/"));
            if (newPath.length() > 2) {
                newPath = newPath.substring(3);
                if (!newPath.startsWith("/") && !newPath.startsWith("..")){
                    path += "/" + newPath;
                } else {
                    cd(newPath);
                }
            }
        }
        else if (newPath.startsWith("/")) {
            path = newPath;
        } else {
            path += "/" + newPath;
        }

    }

    public static void main(String[] args) {
        Path path = new Path("/a/b/c/d");
        path.cd("../x");
        System.out.println(path.getPath());
    }
}