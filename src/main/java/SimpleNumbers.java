import java.util.Arrays;

public class SimpleNumbers {

    static int n;
    static boolean[] primes = new boolean[n + 1];

    public static void fillSieve() {
        Arrays.fill(primes, true);
        primes[0] = primes[1] = false;
        for (int i = 2; i < primes.length; ++i) {
            if (primes[i]) {
                for (int j = 2; i * j < primes.length; ++j) {
                    primes[i * j] = false;
                }
            }
        }
    }

    public static void main(String[] args) {
        n = 20;
        fillSieve();
    }
}
