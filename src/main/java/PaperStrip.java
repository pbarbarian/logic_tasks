import java.util.ArrayList;
import java.util.List;

public class PaperStrip {
    // for array with no duplicates, difficulty = O(n)
    public static int minPieces(int[] original, int[] desired) {
        int count = 0;
//        List arrays = new ArrayList();
        for (int i = 0; i <= original.length - 1; i++) {
            int index = getIndexOf(original[i], desired);
            int x = 1;
//            String array = String.valueOf(original[i]);
            while (i+1 <= original.length-1 && index+x <=desired.length-1 && original[i + 1] == desired[index + x]) {
//                array += String.valueOf(desired[index+x]);
                x++;
                i++;
            }
            count++;
//            arrays.add(array);
        }
        return count;
    }

    private static int getIndexOf(int number, int[] array) {
        int position = -1;
        for (int i = 0; i <= array.length - 1; i++) {
            if (number == array[i]) {
                position = i;
                break;
            }
        }
        return position;
    }

    public static void main(String[] args) {
        int[] original = new int[]{1, 4, 3, 2};
        int[] desired = new int[]{1, 2, 4, 3};
        System.out.println(PaperStrip.minPieces(original, desired));
    }
}
