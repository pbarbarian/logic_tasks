import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Needle {
    public static int count(String needle, InputStream haystack) throws Exception {
        List<String> lines = new BufferedReader(new InputStreamReader(haystack))
                .lines().collect(Collectors.toList());

        int countLines = 0;
        for (String line : lines) {
            int count = countOccurences(line, needle);
            if (count > 0) {
                countLines++;
            }
        }
        return countLines;
    }

    public static int countOccurences(String text, String find) {
        int index = 0, count = 0, length = find.length();
        while( (index = text.indexOf(find, index)) != -1 ) {
            index += length; count++;
        }
        return count;
    }

    public static void main(String[] args) throws Exception {
        String inMessage = "Hello, there!\nHow are you today?\nYes, you over there.";

        try(InputStream inStream = new ByteArrayInputStream(inMessage.getBytes())) {
            System.out.println(Needle.count("there", inStream));
        }
    }
}