public class SortedSearch {

    public static int countNumbers(int[] sortedArray, int lessThan) {
        int lo = 0;
        int hi = sortedArray.length - 1;
        while (lo <= hi) {
            // Key is in a[lo..hi] or not present.
            int mid = lo + (hi - lo) / 2;
            if (lessThan < sortedArray[mid]) hi = mid - 1;
            else if (lessThan > sortedArray[mid]) lo = mid + 1;
            else return mid;
        }
        return lo;
    }

    public static void main(String[] args) {
        System.out.println(SortedSearch.countNumbers(new int[]{1, 3, 5, 7}, 4));
    }
}