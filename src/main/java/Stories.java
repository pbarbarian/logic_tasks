public class Stories {

    public static int combinations(int numberOfStories) {
        return fibIteration(numberOfStories + 1);
    }

    private static int fibIteration(int n)  {
        if (n < 3) {
            return 1;
        }
        int res0 = 1;
        int resn = 1;
        int res = 0;
        for (int i = 3; i <= n; i++) {
            res = res0 + resn;
            res0 = resn;
            resn = res;
            if (res < 0 ) {
                return res;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(combinations(3));
    }
}