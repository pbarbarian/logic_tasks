import java.util.concurrent.Callable;

interface IBird {
    Egg lay();
}

public class Chicken implements IBird{
    public Chicken() {

    }

    public static void main(String[] args) throws Exception {
        Chicken chicken = new Chicken();
        Egg egg = new Egg(Chicken::new);
        egg.hatch();
        egg.hatch();
        System.out.println(chicken instanceof IBird);
    }

    @Override
    public Egg lay() {
        return new Egg(Chicken::new);
    }
}

class Egg {
    Callable<IBird> bird;
    private boolean alreadyHatched;

    public Egg(Callable<IBird> createBird) {
        bird = createBird;
    }

    public IBird hatch() throws Exception {
        if (alreadyHatched) {
            throw new IllegalStateException();
        }
        alreadyHatched = true;
        return bird.call();
    }
}
