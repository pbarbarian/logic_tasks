import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class PathTest {
    @Test
    public void changeFinalFolder() {
        Path path = new Path("/a/b/c/d");
        path.cd("../x");
        assertThat(path.getPath(), is("/a/b/c/x"));
    }

    @Test
    public void getLevelHigher() {
        Path path = new Path("/a/b/c/d");
        path.cd("..");
        assertThat(path.getPath(), is("/a/b/c"));
    }

    @Test
    public void getSeveralLevelsHigher() {
        Path path = new Path("/a/b/c/d");
        path.cd("../..");
        assertThat(path.getPath(), is("/a/b"));
    }

    @Test
    public void getLevelLower() {
        Path path = new Path("/a/b/c/d");
        path.cd("x");
        assertThat(path.getPath(), is("/a/b/c/d/x"));
    }

    @Test
    public void selectRootDirectory() {
        Path path = new Path("/a/b/c/d");
        path.cd("/");
        assertThat(path.getPath(), is("/"));
    }

    @Test
    public void selectingParentDirectory() {
        Path path = new Path("/a/b/c/d");
        path.cd("/a");
        assertThat(path.getPath(), is("/a"));
    }
}
