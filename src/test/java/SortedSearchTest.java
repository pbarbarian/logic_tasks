import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SortedSearchTest {
    @Test
    public void test1() {
        int count = SortedSearch.countNumbers(new int[]{1, 3, 5, 7}, 4);
        assertThat(count, is(2));
    }

    @Test
    public void arrayLengthIsOdd() {
        int count = SortedSearch.countNumbers(new int[]{1, 3, 5, 7, 19, 35, 48}, 15);
        assertThat(count, is(4));
    }

    @Test
    public void lessThanIsInArray() {
        int count = SortedSearch.countNumbers(new int[]{1, 3, 5, 7, 19, 35, 48}, 19);
        assertThat(count, is(4));
    }

    @Test
    public void test4() {
        int count = SortedSearch.countNumbers(new int[]{1, 3, 5, 7}, 1);
        assertThat(count, is(0));
    }

    @Test
    public void lessThanLowerThat1stNumber() {
        int count = SortedSearch.countNumbers(new int[]{1, 3, 5, 7}, 0);
        assertThat(count, is(0));
    }

    @Test
    public void allElements() {
        int count = SortedSearch.countNumbers(new int[]{1, 3, 5, 7}, 10);
        assertThat(count, is(4));
    }
}
