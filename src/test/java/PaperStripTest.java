import static org.hamcrest.core.Is.is;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class PaperStripTest {

    @Test
    public void test1() {
        int[] original = new int[] {1, 4, 3, 2};
        int[] desired = new int[] {1, 2, 4, 3};
        assertThat(PaperStrip.minPieces(original, desired), is(3));
    }

    @Test
    public void test2() {
        int[] original = new int[] {1, 4, 3, 2, 12, 14, 8};
        int[] desired = new int[] {1, 2, 12, 14, 4, 3, 8};
        assertThat(PaperStrip.minPieces(original, desired), is(4));
    }

    @Test
    public void test3() {
        int[] original = new int[] {1, 4, 3, 2, 12, 14, 8, 9};
        int[] desired = new int[] {1, 2, 12, 14, 4, 3, 8, 9};
        assertThat(PaperStrip.minPieces(original, desired), is(4));
    }

    @Test
    public void test4() {
        int[] original = new int[] {1, 4, 3, 2, 12, 14, 8, 9};
        int[] desired = new int[] {1, 2, 12, 14, 4, 3, 9, 8};
        assertThat(PaperStrip.minPieces(original, desired), is(5));
    }

    @Test
    public void test5() {
        int[] original = new int[] {1, 4, 3, 2, 8, 9, 12, 14};
        int[] desired = new int[] {1, 2, 12, 14, 4, 3, 8, 9};
        assertThat(PaperStrip.minPieces(original, desired), is(5));
    }
}
