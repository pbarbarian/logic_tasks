import org.junit.Test;

import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class TwoSumTest {

    @Test
    public void nullValue() {
        int[] indices = TwoSum.findTwoSum(new int[]{1, 3, 5, 7, 9}, 13);
        assertNull(indices);
    }

    @Test
    public void test1() {
        int[] indices = TwoSum.findTwoSum(new int[]{1, 3, 5, 7, 9}, 12);
        assertThat(indices.length, is(2));
        assertTrue((
                IntStream.of(indices).anyMatch(x -> x == 2) && IntStream.of(indices).anyMatch(x -> x == 3)) ||
                (IntStream.of(indices).anyMatch(x -> x == 1) && IntStream.of(indices).anyMatch(x -> x == 4)));
    }

    @Test
    public void test2() {
        int[] indices = TwoSum.findTwoSum(new int[]{1, 3, 5, 7, 9, 12, 13, 17, 19, 23}, 12);
        assertThat(indices.length, is(2));
        assertTrue((
                IntStream.of(indices).anyMatch(x -> x == 2) && IntStream.of(indices).anyMatch(x -> x == 3)) ||
                (IntStream.of(indices).anyMatch(x -> x == 1) && IntStream.of(indices).anyMatch(x -> x == 4)));
    }

    @Test
    public void negativeValues() {
        int[] indices = TwoSum.findTwoSum(new int[]{-1, -3, -5, 7, 9, 11, 13, 17, 19, 23}, 16);
        assertThat(indices.length, is(2));
        assertTrue((IntStream.of(indices).anyMatch(x -> x == 3) && IntStream.of(indices).anyMatch(x -> x == 4))||
                (IntStream.of(indices).anyMatch(x -> x == 0) && IntStream.of(indices).anyMatch(x -> x == 7)));
    }

    @Test
    public void negativeValuesj() {
        int[] indices = TwoSum.findTwoSum(new int[]{-1, -3, -5, 7, 8, 10, 11, 13, 17, 19, 23}, 16);
        assertThat(indices.length, is(2));
        assertTrue(IntStream.of(indices).anyMatch(x -> x == 0) && IntStream.of(indices).anyMatch(x -> x == 8));
    }
}
